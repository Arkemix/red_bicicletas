var bicicleta = require('../models/bicicleta');

exports.bicicleta_list = async function(req, res){
    bicicleta.find({}, (err, bicicletas) => {
        res.render('bicicletas/index', {bicis: bicicletas});
    });
}

exports.bicicleta_create_get = function(req,res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req,res){
    let bici = new bicicleta( { code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]});
    bicicleta.add(bici);
    res.redirect('/bicicletas/');
}

exports.bicicleta_delete_post = function(req,res){
    bicicleta.removeById({code: req.body.code});
    
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function(req,res){
    bicicleta.findById(req.params.id, function(err, bicicleta){
        if (err){
            res.redirect('/bicicletas');
        }
        res.render('bicicletas/update', {bici: bicicleta});
    });
}

exports.bicicleta_update_post = function(req,res){
    let bici = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.redirect('/bicicletas');
}