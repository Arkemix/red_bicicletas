var express = require('express');
var router = express.Router();
var tokenController = require('../controllers/tokenController');
const { route } = require('./bicicletas');

router.get('/confirmation/:token', tokenController.confirmationGet);


module.exports = router;