// TEST de los ENDPOINT de API
describe("Bicicleta API", () => {
  describe("GET Bicicletas /", () => {
    it("Status 200", () => {
      expect(bicicleta.allbicis.length).toBe(4);

      let apibicicleta = new bicicleta(5, "azul", "montañera", [
        12.851242,
        32.3451345,
      ]);
      bicicleta.add(apibicicleta);

      request.get("http://localhost:3000/api/bicicletas", function (
        error,
        response,
        body
      ) {
        expect(response.statusCode).toBe(200);
      });
      expect(bicicleta.allbicis.length).toBe(5);
    });
  });

  describe("POST Bicicletas /create", () => {
    it("Status 200", (done) => {
      let headers = { "Content-type": "application/json" };
      let a = '{"id":6,"color": "Negro","modelo": "Rockera","lat": 12.158300,"lng": -86.303647}';

      request.post(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/create",
          body: a,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          done();
        }
      );
      expect(bicicleta.allbicis.length).toBe(5);
    });
  });

  
  describe("POST Bicicletas /create", () => {
    it("Status 200", (done) => {
      let headers = { "Content-type": "application/json" };
      let a = '{"id":7,"color": "Blanco","modelo": "Rockera","lat": 12.142300,"lng": -16.323647}';

      request.post(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/create",
          body: a,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          done();
        }
      );
      expect(bicicleta.allbicis.length).toBe(5);
    });
  });

  describe("POST Bicicletas /create", () => {
    it("Status 200", (done) => {
      let headers = { "Content-type": "application/json" };
      let a = '{"id":8,"color": "Amarillo","modelo": "Clasica","lat": 12.142300,"lng": -16.323647}';

      request.post(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/create",
          body: a,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          done();
        }
      );
      expect(bicicleta.allbicis.length).toBe(5);
    });
  });
  describe("DELETE Bicicletas /delete", () => {
    it("Status 204", (done) => {
      let headers = { "Content-type": "application/json" };
      let a = '{"id":6}';

      expect(bicicleta.allbicis.length).toBe(5);
        
      request.delete(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/delete",
          body: a,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(204);
          done();
        }
      );
    });
  });

  describe("DELETE Bicicletas /delete", () => {
    it("Status 204", (done) => {
      let headers = { "Content-type": "application/json" };
      let a = '{"id":7}';

      expect(bicicleta.allbicis.length).toBe(5);
        
      request.delete(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/delete",
          body: a,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(204);
          done();
        }
      );
    });
  });

  describe("DELETE Bicicletas /delete", () => {
    it("Status 204", (done) => {
      let headers = { "Content-type": "application/json" };
      let a = '{"id":8}';

      expect(bicicleta.allbicis.length).toBe(5);
        
      request.delete(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/delete",
          body: a,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(204);
          done();
        }
      );
    });
  });
});
