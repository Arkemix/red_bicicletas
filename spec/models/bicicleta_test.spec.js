const mongoose = require('mongoose');
const bicicleta = require("../../models/bicicleta");
const request = require("request");
const { compileFileClient } = require('pug');



describe('Testing Bicicletas', function(){
  beforeEach(function(done){
    var mongodb = "mongodb+srv://arkemix:Ligiayfogy30!@arkemix-practicecluster.e7t3k.mongodb.net/<dbname>?retryWrites=true&w=majority";
    mongoose.connect(mongodb, {useNewUrlParser:true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console,'Connection Error: '));
    db.once('Open', function(){
      console.log('DB Connected');
      done();
    });
  });

  afterEach(function(done){
    bicicleta.deleteMany({}, function(err,success){
      if(err) console.log(err);
      done();
    });
  });

  describe('Bicicleta.createInstance',() => {
    it('Crea una instancia de la Bicicleta', async(done) => {
      var bici = await bicicleta.createInstance(1,'verde','urbana', [-16.5, 30.1]);
      
      expect(bici.code).toBe(1);
      expect(bici.color).toBe('verde');
      expect(bici.modelo).toBe('urbana');
      expect(bici.ubicacion[0]).toEqual(-16.5);
      expect(bici.ubicacion[1]).toEqual(30.1);
      done();
    })
  });

});
// // Cada vez que se ejecute un test, comenzara con el arreglo vacio para evitar conflictos entre tests.

// // Se espera que el arreglo comience vacio
// describe("bicicleta.allBicis", () => {
//   it("Comienza vacía", () => {
//     expect(bicicleta.allbicis.length).toBe(0);
//   });
// });

// // Se espera que se pueda agregar una nueva bicicleta al arreglo
// describe("bicicletas.add", () => {
//   it("Agregar una Bicicleta", () => {
//     expect(bicicleta.allbicis.length).toBe(0);

//     let a = new bicicleta(1, "azul", "montañera", [12.851242, 32.3451345]);
//     bicicleta.add(a);

//     expect(bicicleta.allbicis.length).toBe(1);
//     expect(bicicleta.allbicis[0]).toBe(a);
//   });
// });
// describe("bicicleta.allBicis", () => {
//   it("Comienza vacía", () => {
//     expect(bicicleta.allbicis.length).toBe(0);
//   });
// });

// describe("bicicletas.add", () => {
//     it("Agregar una Bicicleta", () => {
//       expect(bicicleta.allbicis.length).toBe(1);
  
//       let b = new bicicleta(2, "morado", "montañera", [17.851242, 42.3451345]);
//       bicicleta.add(b);
  
//       expect(bicicleta.allbicis.length).toBe(2);
//       expect(bicicleta.allbicis[1]).toBe(b);
//     });
//   });

// // Se espera que se pueda encontrar la bicicleta por ID
// describe("bicicleta.findByID", () => {
//   it("Encontrar una bicicleta por ID", () => {
//     let c = new bicicleta(3, "rojo", "urbana", [13.851233, -32.3451345]);
//     bicicleta.add(c);
    
//     let targetBici = bicicleta.findById(3);
//     expect(targetBici.id).toBe(c.id);
//     expect(targetBici.color).toBe(c.color);
//     expect(targetBici.modelo).toBe(c.modelo);
//   });
// });
// describe("bicicleta.findByID", () => {
//     it("Encontrar una bicicleta por ID", () => {
//       let d = new bicicleta(4, "verde", "deportiva", [12.851225, 16.3456745]);
//       bicicleta.add(d);
//       let targetBici = bicicleta.findById(4);
//       expect(targetBici.id).toBe(d.id);
//       expect(targetBici.color).toBe(d.color);
//       expect(targetBici.modelo).toBe(d.modelo);
//     });
// });
  
// describe("bicicleta.findByID", () => {
// it("Encontrar una bicicleta por ID", () => {
//     let e = new bicicleta(10, "Cafe", "Clasica", [21.851225, -61.3456745]);
//     bicicleta.add(e);
//     let targetBici = bicicleta.findById(10);
//     expect(targetBici.id).toBe(e.id);
//     expect(targetBici.color).toBe(e.color);
//     expect(targetBici.modelo).toBe(e.modelo);
//     });
// });

// // Se espera que se pueda remover una bicicleta por su ID
// describe("bicicleta.removeByID", () => {
//   it("Remover Biciclet por ID", () => {
//     expect(bicicleta.allbicis.length).toBe(5);
//     let targetBici = bicicleta.findById(3);
//     let replacedBici = bicicleta.findById(4);

//     expect(bicicleta.allbicis[2]).toBe(targetBici);
//     expect(bicicleta.allbicis[3]).toBe(replacedBici);

//     bicicleta.removeById(targetBici.id);

//     expect(bicicleta.allbicis.length).toBe(4);
//     expect(bicicleta.allbicis[2]).toBe(replacedBici);
//   });
// });

// // TEST de los ENDPOINT de API
// describe("Bicicleta API", () => {
//   describe("GET Bicicletas /", () => {
//     it("Status 200", () => {
//       expect(bicicleta.allbicis.length).toBe(4);

//       let apibicicleta = new bicicleta(5, "azul", "montañera", [
//         12.851242,
//         32.3451345,
//       ]);
//       bicicleta.add(apibicicleta);

//       request.get("http://localhost:3000/api/bicicletas", function (
//         error,
//         response,
//         body
//       ) {
//         expect(response.statusCode).toBe(200);
//       });
//       expect(bicicleta.allbicis.length).toBe(5);
//     });
//   });

//   describe("POST Bicicletas /create", () => {
//     it("Status 200", (done) => {
//       let headers = { "Content-type": "application/json" };
//       let a = '{"id":6,"color": "Negro","modelo": "Rockera","lat": 12.158300,"lng": -86.303647}';

//       request.post(
//         {
//           headers: headers,
//           url: "http://localhost:3000/api/bicicletas/create",
//           body: a,
//         },
//         function (error, response, body) {
//           expect(response.statusCode).toBe(200);
//           done();
//         }
//       );
//       expect(bicicleta.allbicis.length).toBe(5);
//     });
//   });

  
//   describe("POST Bicicletas /create", () => {
//     it("Status 200", (done) => {
//       let headers = { "Content-type": "application/json" };
//       let a = '{"id":7,"color": "Blanco","modelo": "Rockera","lat": 12.142300,"lng": -16.323647}';

//       request.post(
//         {
//           headers: headers,
//           url: "http://localhost:3000/api/bicicletas/create",
//           body: a,
//         },
//         function (error, response, body) {
//           expect(response.statusCode).toBe(200);
//           done();
//         }
//       );
//       expect(bicicleta.allbicis.length).toBe(5);
//     });
//   });

//   describe("POST Bicicletas /create", () => {
//     it("Status 200", (done) => {
//       let headers = { "Content-type": "application/json" };
//       let a = '{"id":8,"color": "Amarillo","modelo": "Clasica","lat": 12.142300,"lng": -16.323647}';

//       request.post(
//         {
//           headers: headers,
//           url: "http://localhost:3000/api/bicicletas/create",
//           body: a,
//         },
//         function (error, response, body) {
//           expect(response.statusCode).toBe(200);
//           done();
//         }
//       );
//       expect(bicicleta.allbicis.length).toBe(5);
//     });
//   });
//   describe("DELETE Bicicletas /delete", () => {
//     it("Status 204", (done) => {
//       let headers = { "Content-type": "application/json" };
//       let a = '{"id":6}';

//       expect(bicicleta.allbicis.length).toBe(5);
        
//       request.delete(
//         {
//           headers: headers,
//           url: "http://localhost:3000/api/bicicletas/delete",
//           body: a,
//         },
//         function (error, response, body) {
//           expect(response.statusCode).toBe(204);
//           done();
//         }
//       );
//     });
//   });

//   describe("DELETE Bicicletas /delete", () => {
//     it("Status 204", (done) => {
//       let headers = { "Content-type": "application/json" };
//       let a = '{"id":7}';

//       expect(bicicleta.allbicis.length).toBe(5);
        
//       request.delete(
//         {
//           headers: headers,
//           url: "http://localhost:3000/api/bicicletas/delete",
//           body: a,
//         },
//         function (error, response, body) {
//           expect(response.statusCode).toBe(204);
//           done();
//         }
//       );
//     });
//   });

//   describe("DELETE Bicicletas /delete", () => {
//     it("Status 204", (done) => {
//       let headers = { "Content-type": "application/json" };
//       let a = '{"id":8}';

//       expect(bicicleta.allbicis.length).toBe(5);
        
//       request.delete(
//         {
//           headers: headers,
//           url: "http://localhost:3000/api/bicicletas/delete",
//           body: a,
//         },
//         function (error, response, body) {
//           expect(response.statusCode).toBe(204);
//           done();
//         }
//       );
//     });
//   });
// });
