var mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
var Reserva = require("./reserva");
var Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const crypto = require("crypto");
var saltRounds = 10;

const Token = require("../models/token");
const mailer = require("../mailer/mailer");
const { NotImplemented } = require("http-errors");

const validateEmail = function (email) {
  const re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  return re.test(email);
};

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    required: [true, "El Email es obligatorio"],
    lowercase: true,
    unique: true,
    validate: [validateEmail, "Por favor, ingrese un correo valido"],
    match: [/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/],
  },
  password: {
    type: String,
    required: [true, "El password es obligatorio"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
});

usuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.plugin(uniqueValidator, {
  message: "El {PATH} ya existe con otro usuario",
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
  var reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde: desde,
    hasta: hasta,
    cb: cb,
  });
  // console.log(reserva);
  reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return console.log(err.message);
    }

    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: email_destination,
      subject: "Verificacion de Cuenta",
      text:
        "Hola, \n\n" +
        "Por favor, para verificar su cuenta haga click en el siguiente enlace: \n" +
        "https://bicicletas-redes.herokuapp.com/" +
        "/token/confirmation/" +
        token.token +
        ".\n",
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return console.log(err.message);
      }

      console.log("A verification email has been sent to " + email_destination);
    });
  });
};

usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return console.log(err.message);
    }

    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: email_destination,
      subject: "Reseteo de Password de cuenta",
      text:
        "Hola, \n\n" +
        "Por favor, para resetear el password de su cuenta haga click en el siguiente enlace: \n" +
        "https://bicicletas-redes.herokuapp.com/" +
        "/resetPassword/" +
        token.token +
        ".\n",
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return console.log(err.message);
      }

      console.log("A verification email has been sent to " + email_destination);
    });
    cb(null);
  });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition,callback){
  const self = this;
  console.log(`ID:  ${condition.id}`);
  console.log(`EMAIL:  ${condition.emails[0].value}`);
  self.findOne({
      $or:[
          {'googleId':condition.id},{'email':condition.emails[0].value}
      ]
  }, (err,result)=>{
      if(result){
          callback(err,result)
      }else{
          console.log('------------CONDITION-------------');
          console.log(condition);
          let values = {};
          values.googleId = condition.id;
          values.email = condition.emails[0].value;
          values.name = condition.displayName || 'WITH OUT NAME';
          values.verified = true;
          values.password = crypto.randomBytes(16).toString('hex');
          console.log('------------VALUES-------------');
          console.log(values);
          self.create(values,(err,result)=>{
              if(err) console.log(err);
              return callback(err,result)
          })
      }
  })
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition,callback){
  const self = this;
  console.log(`ID:  ${condition.id}`);
  console.log(`EMAIL:  ${condition.emails[0].value}`);
  self.findOne({
      $or:[
          {'facebookId':condition.id},{'email':condition.emails[0].value}
      ]
  }, (err,result)=>{
      if(result){
          callback(err,result)
      }else{
          let values = {};
          values.facebookId = condition.id;
          values.email = condition.emails[0].value;
          values.name = condition.displayName || 'WITH OUT NAME';
          values.verified = true;
          values.password = crypto.randomBytes(16).toString('hex');
          console.log('------------VALUES-------------');
          console.log(values);
          self.create(values,(err,result)=>{
              if(err) console.log(err);
              return callback(err,result)
          })
      }
  })
}

module.exports = mongoose.model("Usuario", usuarioSchema);
