var mymap = L.map("map").setView([12.12804, -86.26485], 13);

L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
  maxZoom: 18,
}).addTo(mymap);

L.control.scale().addTo(mymap);



async function getBicicletasMarkers(){
  const ID_LENGTH = 5;
  let res = await fetch('http://localhost:3000/api/bicicletas/');
  let data = await res.json();

  data.bicis.forEach(function(bici){
    const id = bici._id.length > ID_LENGTH ? bici._id.substring(0,ID_LENGTH) + '...' : bici._id;
    L.marker(bici.ubicacion, {title:'#'+ id + ' - ' + bici.modelo}).addTo(mymap);
  });
}
getBicicletasMarkers();